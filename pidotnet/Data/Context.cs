namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Domaine.entities;
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<article> articles { get; set; }
        public virtual DbSet<bus> buses { get; set; }
        public virtual DbSet<com_artilce> com_artilce { get; set; }
        public virtual DbSet<commentaire> commentaires { get; set; }
        public virtual DbSet<condidature> condidatures { get; set; }
        public virtual DbSet<conge> conges { get; set; }
        public virtual DbSet<employe> employes { get; set; }
        public  DbSet<evaluation> evaluations { get; set; }
        public  DbSet<evaluationcriteria> evaluationcriterias { get; set; }
        public virtual DbSet<facture> factures { get; set; }
         public  DbSet<formation> formations { get; set; }
        public  DbSet<job> jobs { get; set; }
        public  DbSet<jobskillreq> jobskillreqs { get; set; }
        public virtual DbSet<mission> missions { get; set; }
        public  DbSet<planing> planings { get; set; }
        public  DbSet<skill> skills { get; set; }
        public virtual DbSet<t_project> t_project { get; set; }
        public virtual DbSet<t_task> t_task { get; set; }
        public virtual DbSet<t_timesheet> t_timesheet { get; set; }
        public virtual DbSet<team> teams { get; set; }
        public  DbSet<userskill> userskills { get; set; }
        public virtual DbSet<workday> workdays { get; set; }
        public virtual DbSet<Class1> class1s { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.employe")]
    public partial class employe
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public employe()
        {
            articles = new HashSet<article>();
            com_artilce = new HashSet<com_artilce>();
            commentaires = new HashSet<commentaire>();
            condidatures = new HashSet<condidature>();
            conges = new HashSet<conge>();
            planings = new HashSet<planing>();
            userskills = new HashSet<userskill>();
            evaluations = new HashSet<evaluation>();
            evaluations1 = new HashSet<evaluation>();
            t_task = new HashSet<t_task>();
        }

        [Key]
        public int E_ID { get; set; }

        [StringLength(255)]
        public string E_ADDRESS { get; set; }

        public int? E_AGE { get; set; }

        [StringLength(255)]
        public string E_DEPARTEMENT { get; set; }

        [StringLength(255)]
        public string E_IMAGE { get; set; }

        [StringLength(255)]
        public string E_LASTNAME { get; set; }

        [StringLength(255)]
        public string E_LOGIN { get; set; }

        [StringLength(255)]
        public string E_MAIL { get; set; }

        [StringLength(255)]
        public string E_NAME { get; set; }

        public int? E_NBRJOURCONGES { get; set; }

        [StringLength(255)]
        public string E_PASSWORD { get; set; }

        [StringLength(255)]
        public string E_PROJET { get; set; }

        [StringLength(255)]
        public string E_ROLE { get; set; }

        public double? E_SALAIRE { get; set; }

        [StringLength(255)]
        public string E_TEAM { get; set; }

        public int? id_bus { get; set; }

        public int? FK_JOB_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<article> articles { get; set; }

        public virtual bus bus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<com_artilce> com_artilce { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<commentaire> commentaires { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<condidature> condidatures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<conge> conges { get; set; }

        public virtual job job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<planing> planings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<userskill> userskills { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<evaluation> evaluations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<evaluation> evaluations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<t_task> t_task { get; set; }
    }
}

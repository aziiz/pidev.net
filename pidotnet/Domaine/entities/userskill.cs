namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.userskill")]
    public partial class userskill
    {
        public int id { get; set; }

        public int? level { get; set; }

        public int? FK_EMP_ID { get; set; }

        public int? FK_SKILL_ID { get; set; }

        public virtual employe employe { get; set; }

        public virtual skill skill { get; set; }
    }
}

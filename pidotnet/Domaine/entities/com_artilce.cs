namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.com_artilce")]
    public partial class com_artilce
    {
        [Key]
        public int COM_ART_ID { get; set; }

        [StringLength(255)]
        public string COM_ART_TEXTE { get; set; }

        public int? id_Article { get; set; }

        public int? id_employe { get; set; }

        public virtual article article { get; set; }

        public virtual employe employe { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.conges")]
    public partial class conge
    {
        [Key]
        public int CON_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CON_DATEDEBUT { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CON_DATEFIN { get; set; }

        public int? CON_DUREE { get; set; }

        [StringLength(255)]
        public string CON_ETAT { get; set; }

        [StringLength(255)]
        public string CON_TYPE { get; set; }

        public int? id_employe { get; set; }

        public virtual employe employe { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.t_project")]
    public partial class t_project
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public t_project()
        {
            t_task = new HashSet<t_task>();
            t_timesheet = new HashSet<t_timesheet>();
        }

        [Key]
        public int P_ID { get; set; }

        [StringLength(255)]
        public string P_DESCRIPTION { get; set; }

        public int? P_DURATION { get; set; }

        public DateTime? P_ENDTDATE { get; set; }

        [StringLength(255)]
        public string P_NAME { get; set; }

        public DateTime? P_STARTDATE { get; set; }

        [StringLength(255)]
        public string P_STATUS { get; set; }

        [StringLength(255)]
        public string technologie { get; set; }

        public int? FK_T_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<t_task> t_task { get; set; }

        public virtual team team { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<t_timesheet> t_timesheet { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.evaluation")]
    public partial class evaluation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public evaluation()
        {
            evaluationcriterias = new HashSet<evaluationcriteria>();
        }

        public int id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(255)]
        public string dependability { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int note { get; set; }

        [StringLength(255)]
        public string ponctuality { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        public int? receiver_E_ID { get; set; }

        public int? sender_E_ID { get; set; }

        public virtual employe employe { get; set; }

        public virtual employe employe1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<evaluationcriteria> evaluationcriterias { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.article")]
    public partial class article
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public article()
        {
            com_artilce = new HashSet<com_artilce>();
        }

        [Key]
        public int ART_ID { get; set; }

        [StringLength(255)]
        public string ART_DESC { get; set; }

        [StringLength(255)]
        public string ART_IMG { get; set; }

        [StringLength(255)]
        public string ART_THEME { get; set; }

        public int? id_employe { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<com_artilce> com_artilce { get; set; }

        public virtual employe employe { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.mission")]
    public partial class mission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mission()
        {
            commentaires = new HashSet<commentaire>();
            condidatures = new HashSet<condidature>();
            factures = new HashSet<facture>();
        }

        [Key]
        public int M_ID { get; set; }

        public DateTime? M_DATEDEBUT { get; set; }

        public DateTime? M_DATEFIN { get; set; }

        [StringLength(255)]
        public string departement { get; set; }

        [StringLength(255)]
        public string M_DEVICE { get; set; }

        public float? M_FRAISHEBERGEMENT { get; set; }

        public float? M_FRAISJOURNEE { get; set; }

        public float? M_FRAISRESTAURATION { get; set; }

        public float? M_FRAISTRANSPORT { get; set; }

        [StringLength(255)]
        public string M_NAME { get; set; }

        [StringLength(255)]
        public string M_PAYS { get; set; }

        public int? M_RATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<commentaire> commentaires { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<condidature> condidatures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<facture> factures { get; set; }
    }
}

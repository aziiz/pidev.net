namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.workday")]
    public partial class workday
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public workday()
        {
            t_timesheet = new HashSet<t_timesheet>();
        }

        [Key]
        public int WD_ID { get; set; }

        public DateTime? WD_DATE { get; set; }

        public int? FK_T_ID { get; set; }

        public virtual t_task t_task { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<t_timesheet> t_timesheet { get; set; }
    }
}

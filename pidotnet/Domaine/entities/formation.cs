namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.formation")]
    public partial class formation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public formation()
        {
            planings = new HashSet<planing>();
        }

        [Key]
        public int Id_Formation { get; set; }

        [StringLength(255)]
        public string Contact_Formateur { get; set; }

        [StringLength(255)]
        public string Technologie { get; set; }

        [StringLength(255)]
        public string Etat_Formation { get; set; }

        [StringLength(255)]
        public string Formateur_Formation { get; set; }

        [StringLength(255)]
        public string Image { get; set; }

        [StringLength(255)]
        public string Type_Formation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<planing> planings { get; set; }
    }
}

namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.t_task")]
    public partial class t_task
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public t_task()
        {
            workdays = new HashSet<workday>();
        }

        [Key]
        public int T_ID { get; set; }

        public DateTime? creationDate { get; set; }

        public DateTime? endDate { get; set; }

        public int? T_ESTIMATEDCOST { get; set; }

        public int? T_logged { get; set; }

        [StringLength(255)]
        public string T_NAME { get; set; }

        public int? T_REALCOST { get; set; }

        public long? T_REMINING { get; set; }

        [StringLength(255)]
        public string status { get; set; }

        public int? employe_E_ID { get; set; }

        public int? FK_P_ID { get; set; }

        public virtual employe employe { get; set; }

        public virtual t_project t_project { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<workday> workdays { get; set; }
    }
}

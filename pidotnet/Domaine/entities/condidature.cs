namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.condidature")]
    public partial class condidature
    {
        [Key]
        public int C_ID { get; set; }

        [StringLength(255)]
        public string departement { get; set; }

        [StringLength(255)]
        public string LETTREDEMOTIVATION { get; set; }

        public int? FK_EMP_ID { get; set; }

        public int? FK_M_ID { get; set; }

        public virtual mission mission { get; set; }

        public virtual employe employe { get; set; }
    }
}

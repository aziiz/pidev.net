namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.jobskillreq")]
    public partial class jobskillreq
    {
        public int id { get; set; }

        public int? level { get; set; }

        public int? FK_JOB_ID { get; set; }

        public int? FK_SKILL_ID { get; set; }

        public virtual job job { get; set; }

        public virtual skill skill { get; set; }
    }
}

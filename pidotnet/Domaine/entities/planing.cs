namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.planing")]
    public partial class planing
    {
        [Key]
        public int Id_Planing { get; set; }

        [StringLength(255)]
        public string Lieu { get; set; }

        public DateTime? Date_Formation { get; set; }

        public int? E_ID { get; set; }

        public int? id_formation { get; set; }

        public virtual employe employe { get; set; }

        public virtual formation formation { get; set; }
    }
}

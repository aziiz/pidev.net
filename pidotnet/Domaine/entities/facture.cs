namespace Domaine.entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("imputation.facture")]
    public partial class facture
    {
        [Key]
        public int F_ID { get; set; }

        public int? F_MATRICULEFISCALE { get; set; }

        public float? M_MONTANT { get; set; }

        public int? FK_M_ID { get; set; }

        public string F_IMAGE { get; set; }

        public virtual mission mission { get; set; }

    }
}

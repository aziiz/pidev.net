﻿using Domaine.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pidotnet.Models
{
    public class Evaluation
    {
        public int id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(255)]
        public string dependability { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int note { get; set; }

        [StringLength(255)]
        public string ponctuality { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        public int? receiver_E_ID { get; set; }

        public int? sender_E_ID { get; set; }

        public virtual employe employe { get; set; }

        public virtual employe employe1 { get; set; }
    }
}
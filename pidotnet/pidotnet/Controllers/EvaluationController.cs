﻿using Domaine.entities;
using pidotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace pidotnet.Controllers
{
    public class EvaluationController : Controller
    {
        // GET: Evaluation
        public ActionResult Index()
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("http://localhost:60573");
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = Client.GetAsync("rest/evaluations/show").Result;
            ViewBag.result = response.Content.ReadAsAsync<IEnumerable<Evaluation>>().Result;

            if (response.IsSuccessStatusCode)
            {
             //   ViewBag.result = response.Content.ReadAsStringAsync<IEnumerable<Evaluation>>().Result;
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<Evaluation>>().Result;
            }
            else
            {
                ViewBag.result = "erourou";
            }


            return View();
        }
    }
}